" Configuration file for vim
set modelines=0		" CVE-2007-2438

" Normally we use vim-extensions. If you want true vi-compatibility
" remove change the following statements
set nocompatible	" Use Vim defaults instead of 100% vi compatibility
set backspace=2		" more powerful backspacing

" Use pathogen plugin to easily modify the runtime path to include all
" plugins under the ~./vim/bundle directory
" http://www.vim.org/scripts/script.php?script_id=2332
call pathogen#infect()
call pathogen#helptags()

" Faster command mode access
nmap <Space> :
nmap ; :


" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

" Don't write backup file if vim is being called by "crontab -e"
au BufWrite /private/tmp/crontab.* set nowritebackup
" Don't write backup file if vim is being called by "chpass"
au BufWrite /private/etc/pw.* set nowritebackup

" Make vim save and load the folding of the document each time it loads"
" also places the cursor in the last place that it was left."
au BufWinLeave * mkview
au BufWinEnter * silent loadview

" Hide buffers vs closing when opening a new file
set hidden

" status line
set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [ASCII=\%03.3b]\ [HEX=\%02.2B]\ [POS=%04l,%04v]\ [%p%%]\ [LEN=%L]
set laststatus=2

" Searching
set ignorecase   " ignore case when searching
set smartcase    " ignore case if search pattern is lowercase, otherwise case sensitive
set hlsearch     " highlight search terms
set incsearch    " show matches as you type

syntax enable

" Tab handling
" set expandtab
set tabstop=2
set shiftwidth=2

" Change j/k to move by visible lines, even for wrapped lines
nnoremap j gj
nnoremap k gk

" Code indenting
set autoindent
set smartindent

" Briefly jump to matching bracket
set showmatch

set nowrap
set relativenumber

" Allow mouse to set cursor position
set mouse=a

" Easy window navigation - remove need for <C-w>
function! WinMove(key) 
  let t:curwin = winnr()
  exec "wincmd ".a:key
  if (t:curwin == winnr()) "we havent moved
    if (match(a:key,'[jk]')) "were we going up/down
      wincmd v
    else 
      wincmd s
    endif
    exec "wincmd ".a:key
  endif
endfunction
                                           
map <leader>h              :call WinMove('h')<cr>
map <leader>k              :call WinMove('k')<cr>
map <leader>l              :call WinMove('l')<cr>
map <leader>j              :call WinMove('j')<cr>

map <leader>wc             :wincmd q<cr>
map <leader>wr             <C-W>r

map <leader>H              :wincmd H<cr>
map <leader>K              :wincmd K<cr>
map <leader>L              :wincmd L<cr>
map <leader>J              :wincmd J<cr>

" Use <F2> to toggle display of NERDTree
noremap <F2> :NERDTreeToggle<CR>
filetype plugin indent on
syntax on

" Window movement
map <Leader>w <C-w>w

" Emacs like beginning and end of line
imap <C-e> <C-o>$
imap <C-a> <C-o>^

" Center search matches when jumping
map N Nzz
map n nzz

" Edit another file in the same directory as the current file
" uses expression to extract path from current file's path
map <Leader>e :e <C-R>=expand("%:p:h") . '/'<CR>
map <Leader>s :split <C-R>=expand("%:p:h") . '/'<CR>
map <Leader>v :vnew <C-R>=expand("%:p:h") . '/'<CR>

" Needed for Colorscheme Calmar256-dark 
set t_Co=256
colorscheme calmar256-dark
" set background=dark
" colorscheme solarized
