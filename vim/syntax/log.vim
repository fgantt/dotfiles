" This creates a keyword ERROR and puts it in the highlight group called logError
syntax keyword logError ERROR
" This creates a match on the date and puts in the highlight group called logDate.  The
" nextgroup and skipwhite makes vim look for logTime after the match
syntax match logDate "/^\d\{4}-\d\{2}-\d\{2}/" nextgroup=logTime skipwhite

" This creates a match on the time (but only if it follows the date)
syntax match logTime "/\d\{2}:\d\{2}:\d\{2},\d\{3}/"

" Now make them appear:
" Link just links logError to the colouring for error
hi link logError Error
" Def means default colour - colourschemes can override
hi def logDate guibg=yellow guifg=blue
hi def logTime guibg=green guifg=white

